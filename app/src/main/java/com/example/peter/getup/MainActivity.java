package com.example.peter.getup;

import android.content.Context;
import android.app.PendingIntent;
import android.content.Intent;
import android.hardware.SensorManager;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import static android.app.AlarmManager.INTERVAL_DAY;
import static android.app.AlarmManager.RTC_WAKEUP;

import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity { //test test
    private Button ChangeTimeButton;
    private TextView ClockView;

    private NumberPicker NPHours, NPMinutes;

    private int hours, minutes;
    private String ClockText;




    private PendingIntent alarmIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        ClockView = (TextView) findViewById(R.id.Clockview);

        ChangeTimeButton = (Button) findViewById(R.id.ChangeTimeButton);
        ChangeTimeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, ChooseTime.class);
                startActivity(intent);


            }
        });

        hours = 0;
        minutes =0;



        NPHours = (NumberPicker) findViewById(R.id.hourPicker);
        NPHours.setMaxValue(23);
        NPHours.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker numberPicker, int i, int i1) {
                updateTime();

            }
        });
        NPMinutes = (NumberPicker) findViewById(R.id.minutePicker);
        NPMinutes.setMaxValue(59);
        NPMinutes.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker numberPicker, int i, int i1) {
                updateTime();
            }
        });


        //Incredibly inefficient but works passably
        Timer t = new Timer();
        t.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                Calendar rightNow = Calendar.getInstance();
                int currenthour = rightNow.get(Calendar.HOUR_OF_DAY);
                int currentmin = rightNow.get(Calendar.MINUTE);

                int minsUntil = (hours - currenthour) * 60 + minutes - currentmin;
                if(minsUntil <= 0){
                    //when alarm goes off>>>
                    startTimer();
                }

            }
        }, 0, 10000);





    }
    public void updateTime(){
        minutes = NPMinutes.getValue();
        hours = NPHours.getValue();
        String viewMinutes = String.valueOf(minutes);
        String viewhours = String.valueOf(hours);
        if(minutes < 10){
            viewMinutes = 0 + "" + minutes;
        }

        String finalview = viewhours+":"+viewMinutes;
        ClockView.setText(finalview);

    }

    AlarmManager am;
    Timer t;
    AlarmPlayer alarmPlayerSingleton;
    public void startTimer(){
        if(am != null) {
            return;
        }
        t = new Timer();
        am = new AlarmManager((SensorManager) getSystemService(Context.SENSOR_SERVICE), this);
        alarmPlayerSingleton = new AlarmPlayer();
        t.scheduleAtFixedRate(alarmPlayerSingleton, 1000, 1000);
    }
    class AlarmPlayer extends TimerTask{
        public MediaPlayer mp = MediaPlayer.create(getApplicationContext(), R.raw.alarm);
        AlarmPlayer(){
            super();
            mp.setLooping(true);
        }
        @Override
        public void run() {
            mp.start();
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) { //TODO vary volume
                return;
            }
            mp.pause();
        }
    }
    public void setAlarmVolume(double volume){
        Log.d("ma", "Volume is: " + volume);
        alarmPlayerSingleton.mp.setVolume((float)(volume / 3.0), (float)( volume / 3.0));
    }
    public void endAlarm(){
        Log.d("ma", "Alarm ended!");
        t.cancel();
        t = null;
        am.cancel();
        am = null;
        alarmPlayerSingleton = null;
    }

}
