package com.example.peter.getup;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Detects continuous sweeping motions.
 * Created by Rithwik Sivakumar and Peter Yu on 2/18/2018.
 */

interface SweepListener{
    void receiveSweep(long time);
}

public class SweepDetector implements SensorEventListener {
    private Sensor accelSensor;
    private Sensor gyroSensor;
    private SweepListener callback;
    public SweepDetector(SensorManager sm, SweepListener callback) throws Exception{
        accelSensor = sm.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        gyroSensor = sm.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
        if(accelSensor == null )
            throw new Exception ("No accelerometer");
        if(gyroSensor == null )
            throw new Exception ("No gyroscope");

        sm.registerListener(this, accelSensor, SensorManager.SENSOR_DELAY_GAME);
        sm.registerListener(this, gyroSensor, SensorManager.SENSOR_DELAY_GAME);

        Log.v("adssd", "sweep constructed");

        this.callback = callback;
    }

    /**
     * @param type true for speed, false for direction
     */
    Boolean streakType;
    int streakLength = 0;
    final int streakLenPrereq = 5;
    boolean sent = false;
    int increment = 0;
    private void endEvent(boolean type){
        if(streakType == null)
            streakType = type;
        else if(streakType.equals(type)) {
            streakLength++;
            if(streakLength > streakLenPrereq && !sent){
                long time = speedEvents.get(speedEvents.size() - 1).t - speedEvents.get(0).t;
                /*
                for(int c = 1; c < speedEvents.size() - 1; c++){
                    sumSpeed += speedEvents.get(c).speed * (speedEvents.get(c).t - speedEvents.get(c - 1).t);
                }

                Log.d("ee", "sumspeed:" + sumSpeed + " time:" + time);*/
                callback.receiveSweep(time/* sumSpeed / time*/ ); //TODO speed doesn't work, so just using time for now
                sent = true;
                increment++;
            }
        }else{
            streakType = type;
            streakLength = 0;
            sent = false;
        }
        speedEvents = null;
        directionEvents = null;
    }

    private class SpeedEvent{public final float speed; public final long t; public SpeedEvent(float speed, long t){this.speed = speed; this.t = t;}}
    private class DirectionEvent{public final float x, y, z; public final long t; public DirectionEvent(float x, float y, float z, long t){this.x = x; this.y = y; this.z = z; this.t = t;}}
    private ArrayList<SpeedEvent> speedEvents;
    private ArrayList<DirectionEvent> directionEvents;
    final int speedBoxcarTime = 4; //TODO for now, not time but just number of recordings
    final float avgSpeedStdDevThreshold = 1f; //TODO calibrate these thresholds
    final int directionBoxcarTime = 4; //TODO for now, not time but just number of recordings
    final float avgAngularSpeedThreshold = .5f;
    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        if(speedEvents == null || directionEvents == null){
            speedEvents = new ArrayList<SpeedEvent>(200);
            directionEvents = new ArrayList<DirectionEvent>(200);
        }

        long currentTime = System.currentTimeMillis();

        if(sensorEvent.sensor == accelSensor){
            speedEvents.add(new SpeedEvent(
                    (float) Math.sqrt(
                            sensorEvent.values[0] * sensorEvent.values[0] +
                            sensorEvent.values[0] * sensorEvent.values[1] +
                            sensorEvent.values[0] * sensorEvent.values[2]
                    ),
                    currentTime
            ));
            int s = speedEvents.size();
            float variance = 0;
            for(
                    int c = s - 2;
                    c >=0 && /*speedEvents.get(c).t + speedBoxcarTime < currentTime*/ s - c < speedBoxcarTime;
                    c--
                    ){
                variance += Math.pow(speedEvents.get(c).speed - speedEvents.get(s - 1).speed, 2);
            }
            if(Math.sqrt(variance) > avgSpeedStdDevThreshold)
                endEvent(true);
        }else if(sensorEvent.sensor == gyroSensor){
            directionEvents.add(new DirectionEvent(
                    sensorEvent.values[0],
                    sensorEvent.values[1],
                    sensorEvent.values[2],
                    currentTime
            ));
            int s = directionEvents.size();
            float sumAngularSpeed = 0;
            int numCounted = 0;
            for(
                    int c = s - 2;
                    c >= 0 && /*directionEvents.get(c).t + directionBoxcarTime < currentTime*/s - c < directionBoxcarTime;
                    c--
                    ) {
                DirectionEvent event = directionEvents.get(c);
                sumAngularSpeed += (float) Math.sqrt(event.x * event.x + event.y * event.y + event.z * event.z); //TODO see if this is the right way to do this
                numCounted++;
            }
            if(sumAngularSpeed / numCounted > avgAngularSpeedThreshold){
                endEvent(false);
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }
}
