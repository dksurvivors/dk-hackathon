package com.example.peter.getup;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.TimePicker;
import android.widget.Toast;

import java.sql.Time;

public class ChooseTime extends AppCompatActivity {
    private Button CancelButton, SetTimeButton;

    private int hours, minutes;

    private TimePicker mytimePicker;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_time);

        CancelButton = (Button) findViewById(R.id.CancelButton);
        CancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent returnIntent = getIntent();
                setResult(RESULT_OK,returnIntent);
                finish();
            }
        });

        SetTimeButton = (Button) findViewById(R.id.SetTimeButton);
        SetTimeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent returnIntent = getIntent();
                returnIntent.putExtra("hours",mytimePicker.getHour());
                returnIntent.putExtra("minutes", mytimePicker.getMinute());
                setResult(RESULT_OK, returnIntent);
                finish();
            }
        });

     //   mytimePicker = (TimePicker) findViewById(R.id.myTimePicker);

    }
}
