package com.example.peter.getup;

import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.util.Log;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Peter on 2/18/2018.
 */

public class AlarmManager implements SweepListener{
    static SweepDetector sd;
    MainActivity callback;
    UpdateVolume volUpdSingleton;
    Timer t;
    public AlarmManager(SensorManager sm, MainActivity callback){
        try {
            sd = new SweepDetector(sm, this);
        }catch(Exception ex){
            Log.e("SweepDetector", ex.toString());
        }

        this.callback = callback;
        t = new Timer();
        volUpdSingleton = new UpdateVolume();
        t.scheduleAtFixedRate(volUpdSingleton, 1000, 1000);
    }
    public void cancel(){
        t.cancel();
    }
    class UpdateVolume extends TimerTask {
        double volume = 1;
        final double maxVol = 4;
        final double volIncreaseIncrement = .1;
        final double volDecreaseIncrement = .2;
        final long gracePeriodDuration = 4000;
        long gracePeriodEnd = 0;

        @Override
        public void run() {
            Log.d("am","Vol updated!");
            long currentTime = System.currentTimeMillis();
            if(gracePeriodEnd == 0 || gracePeriodEnd > currentTime) { //if no grace period left increase vol
                volume += volIncreaseIncrement;
            }
            if(volume <= 0){
                callback.endAlarm();
            }
            callback.setAlarmVolume(volume);

        }

        public void oneMotionDone(){
            gracePeriodEnd = System.currentTimeMillis() + gracePeriodDuration;
            volume -= volDecreaseIncrement;
            callback.setAlarmVolume(volume);
        }
    }

    @Override
    public void receiveSweep(long time /*, double speed*/) {//TODO speed doesn't work, so just using time for now
        if(time < 2000){
            volUpdSingleton.oneMotionDone();
        }
    }
}
